#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define internal static

#define ARRAY_COUNT(x) (sizeof(x) / sizeof((x)[0]))

typedef uint32_t u32;
typedef u32 b32;

#define true 1
#define false 0

typedef struct {
  u32 length;
  char* buf;
} String;

#define STR(s) string_wrap(sizeof(s) - 1, s)

internal String
string_wrap(u32 length, char* buf) {
  String s = {.length = length, .buf = buf};
  return s;
}

internal String
string_get_line(String s) {
  String result = {};
  for (u32 i = 0; i < s.length; ++i) {
    if (s.buf[i] == '\n') {
      result.buf = s.buf;
      result.length = i + 1;
      break;
    }
  }
  return result;
}

internal b32
string_equals(String a, String b) {
  b32 result = false;
  if (a.length == b.length) {
    result = true;
    for (u32 i = 0; i < a.length; ++i) {
      if (a.buf[i] != b.buf[i]) {
        result = false;
        break;
      }
    }
  }
  return result;
}

internal b32
string_starts_with(String a, String prefix) {
  b32 result = false;
  if (a.length >= prefix.length) {
    result = true;
    for (u32 i = 0; i < prefix.length; ++i) {
      if (a.buf[i] != prefix.buf[i]) {
        result = false;
        break;
      }
    }
  }
  return result;
}

internal String
string_eat_front(String a, u32 length) {
  a.length -= length;
  a.buf += length;
  return a;
}

typedef enum {
  kResponse_Code,
  kResponse_Value,
  kResponse_Done,
} ResponseState;

int
main() {
  int s = socket(AF_UNIX, SOCK_STREAM, 0);
  if (s >= 0) {
    struct sockaddr_un addr = {.sun_family = AF_UNIX};
    strcpy(addr.sun_path, "/home/btolsch/.dropbox/command_socket");
    int ret = connect(s, (struct sockaddr*)&addr, sizeof(addr));
    if (ret == 0) {
#define EXP(s) s, sizeof(s) - 1
      ret = write(s, EXP("get_dropbox_status\ndone\n"));
#undef EXP
      if (ret > 0) {
        char buf[4000];
        String response = {.buf = buf};
        String value = {};
        b32 good_response = false;
        ResponseState expecting = kResponse_Code;
        while (!good_response) {
          if (response.length >= sizeof(buf)) {
            break;
          }
          ret = read(s, response.buf + response.length, sizeof(buf) - response.length);
          if (ret < 0) {
            if (errno != EINTR) {
              break;
            }
          } else if (ret == 0) {
            break;
          } else {
            response.length += ret;
            String line;
            while (!good_response && (line = string_get_line(response)).length) {
              switch (expecting) {
                case kResponse_Code: {
                  if (string_equals(line, STR("ok\n"))) {
                    expecting = kResponse_Value;
                    response = string_eat_front(response, line.length);
                  }
                } break;
                case kResponse_Value: {
                  if (string_starts_with(line, STR("status\t"))) {
                    value = string_eat_front(line, 7);
                    expecting = kResponse_Done;
                    response = string_eat_front(response, line.length);
                  }
                } break;
                case kResponse_Done: {
                  if (string_starts_with(line, STR("done\n"))) {
                    good_response = true;
                  }
                } break;
              }
            }
          }
        }
        if (good_response) {
          // NOTE(btolsch): Good response is:
          // ok\n
          // status\t<value>[\t<value>[...]]\n
          // done\n
          printf("%.*s", value.length, value.buf);
        } else {
          printf("error reading response\n");
        }
      } else {
        printf("write() error\n");
      }
    } else {
      printf("connect() error\n");
    }
  } else {
    printf("socket() error\n");
  }
  return 0;
}
