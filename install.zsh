#!/bin/zsh

single_key_prompt() {
  old_stty=$(stty -g)
  stty raw -echo; answer=$(head -c 1); stty $old_stty
  echo $answer
}

SELF_DIR=${0:A:h}
pushd $SELF_DIR

if ! crontab -l | grep -q "wall\.sh"; then
  echo -n "add 'wall.sh' to crontab? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    EDITOR="vi -c 'normal Go*/15 * * * * /home/btolsch/bin/wall.sh'" crontab -e
  fi
fi

echo -n "check root crontab for 'pacman -Sy'? [y/N] "
answer=$(single_key_prompt)
echo
if [ "$answer" = "y" -o "$answer" = "Y" ]; then
  if ! sudo crontab -l | grep -q "pacman -Sy"; then
    echo -n "add 'pacman -Sy' to root crontab? [Y/n] "
    answer=$(single_key_prompt)
    echo
    if [ "$answer" != "n" -a "$answer" != "N" ]; then
      sudo EDITOR="vi -c 'normal Go*/15 * * * * pacman -Sy'" crontab -e
    fi
  fi
fi

pamd_login=$(cat /etc/pam.d/login)
if ! (echo "$pamd_login" | sed -n '/^auth\s\+optional\s+pam_gnome_keyring.so$/q0;q1') && \
     (echo "$pamd_login" | sed -n '/^session\s\+optional\s+pam_gnome_keyring.so auto_start$/q0;q1') && \
     pacman -Q gnome-keyring &>/dev/null; then
  echo -n "add gnome-keyring settings to /etc/pam.d/login? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    EDITOR="vi -c 'normal Goauth       optional     pam_gnome_keyring.sosession    optional     pam_gnome_keyring.so auto_start'" sudoedit /etc/pam.d/login
    sudo pacman -Ss gnome-keyring
  fi
fi

if ! git remote -v | grep -q 'git@gitlab.com'; then
  echo -n "add ssh git remote? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [[ "$answer" != "n" && "$answer" != "N" ]]; then
    git remote add origin-ssh 'git@gitlab.com:btolsch/dotfiles-arch.git'
  fi
fi

if ! which dropbox_status &>/dev/null || [[ dropbox_status.c -nt $(which dropbox_status) ]]; then
  echo -n "install dropbox_status? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [[ "$answer" != "n" && "$answer" != "N" ]]; then
    clang -O2 dropbox_status.c -o dropbox_status && mv dropbox_status ~/.local/bin/
  fi
fi

PREFIX=$(realpath --relative-to=$HOME/dotfiles $SELF_DIR)
if [ "${platform_symlink_files[.config/nvim/init.vim]}" = "" ]; then
  platform_symlink_files[.config/nvim/init.vim]=".vimrc"
  platform_symlink_prefixes[.config/nvim/init.vim]=$PREFIX
fi
for file in $(ls -d .* | grep -v ".git"); do
  if [ "${platform_symlink_files[$file]}" = "" ]; then
    platform_symlink_files[$file]=$file
    platform_symlink_prefixes[$file]=$PREFIX
  fi
done

popd
